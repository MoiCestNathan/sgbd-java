package main.tp;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class RechercheLivreController {

    @FXML
    private TextField isbnTextField;

    @FXML
    private VBox resultatContainer;

    @FXML
    private Label isbnLabel;

    @FXML
    private Label titreLabel;

    @FXML
    private Label nomAuteurLabel;

    @FXML
    private Label prenomAuteurLabel;

    @FXML
    private Label nomEditeurLabel;

    @FXML
    private Label villeEditeurLabel;

    private DatabaseConnection databaseConnection = new DatabaseConnection();

    @FXML
    private void handleRechercherButtonClick() {
        String isbn = isbnTextField.getText();
        LivreInfo livre = databaseConnection.rechercherLivreParISBN(isbn);
        if (livre != null) {
            isbnLabel.setText(livre.getIsbn());
            titreLabel.setText(livre.getTitre());
            nomAuteurLabel.setText(livre.getNomAuteur());
            prenomAuteurLabel.setText(livre.getPrenomAuteur());
            nomEditeurLabel.setText(livre.getNomEditeur());
            villeEditeurLabel.setText(livre.getVilleEditeur());

            resultatContainer.setVisible(true); // Afficher le conteneur des résultats
        } else {
            resultatContainer.setVisible(false); // Cacher le conteneur des résultats
            isbnLabel.setText("Aucun livre trouvé avec l'ISBN fourni.");
        }
    }
}
