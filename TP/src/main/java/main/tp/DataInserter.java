package main.tp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DataInserter {

    public void insertSampleData() {
        try (Connection connection = DatabaseConnection.getConnection()) {
            // Insertion des éditeurs
            insertEditeur(connection, "EDT001", "Editeur 1", "123 Rue des Livres", "Paris");
            insertEditeur(connection, "EDT002", "Editeur 2", "456 Avenue des Pages", "Lyon");

            // Insertion des auteurs
            insertAuteur(connection, "AUT001", "Martin", "Pierre");
            insertAuteur(connection, "AUT002", "Durand", "Sophie");

            // Insertion des livres
            insertLivre(connection, "ISBN001", "Livre 1", "urlImage1", "AUT001", "EDT001");
            insertLivre(connection, "ISBN002", "Livre 2", "urlImage2", "AUT002", "EDT002");

            System.out.println("Données insérées avec succès.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void insertEditeur(Connection connection, String code, String nom, String adresse, String ville) throws SQLException {
        String sql = "INSERT INTO Editeur (Code, Nom, Adresse, Ville) VALUES (?, ?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, code);
            statement.setString(2, nom);
            statement.setString(3, adresse);
            statement.setString(4, ville);
            statement.executeUpdate();
        }
    }

    private void insertAuteur(Connection connection, String code, String nom, String prenom) throws SQLException {
        String sql = "INSERT INTO Auteurs (Code, Nom, Prenom) VALUES (?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, code);
            statement.setString(2, nom);
            statement.setString(3, prenom);
            statement.executeUpdate();
        }
    }

    private void insertLivre(Connection connection, String isbn, String titre, String urlImage, String auteur, String editeur) throws SQLException {
        String sql = "INSERT INTO Livre (ISBN, Titre, URLImage, Auteur, Editeur) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, isbn);
            statement.setString(2, titre);
            statement.setString(3, urlImage);
            statement.setString(4, auteur);
            statement.setString(5, editeur);
            statement.executeUpdate();
        }
    }
}
