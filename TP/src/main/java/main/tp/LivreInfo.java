package main.tp;

public class LivreInfo {
    private String isbn;
    private String titre;
    private String nomAuteur;
    private String prenomAuteur;
    private String nomEditeur;
    private String villeEditeur;

    public LivreInfo(String isbn, String titre, String nomAuteur, String prenomAuteur, String nomEditeur, String villeEditeur) {
        this.isbn = isbn;
        this.titre = titre;
        this.nomAuteur = nomAuteur;
        this.prenomAuteur = prenomAuteur;
        this.nomEditeur = nomEditeur;
        this.villeEditeur = villeEditeur;
    }
    public LivreInfo() {
        // Constructeur vide
    }

    public String getIsbn() {
        return isbn;
    }

    public String getTitre() {
        return titre;
    }

    public String getNomAuteur() {
        return nomAuteur;
    }

    public String getPrenomAuteur() {
        return prenomAuteur;
    }

    public String getNomEditeur() {
        return nomEditeur;
    }

    public String getVilleEditeur() {
        return villeEditeur;
    }
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setNomAuteur(String nomAuteur) {
        this.nomAuteur = nomAuteur;
    }

    public void setPrenomAuteur(String prenomAuteur) {
        this.prenomAuteur = prenomAuteur;
    }

    public void setNomEditeur(String nomEditeur) {
        this.nomEditeur = nomEditeur;
    }

    public void setVilleEditeur(String villeEditeur) {
        this.villeEditeur = villeEditeur;
    }

}

