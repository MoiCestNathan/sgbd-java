package main.tp;

import java.sql.Connection;
import java.sql.Statement;

public class DatabaseSetup {

    /**
     * Crée les tables nécessaires dans la base de données.
     *
     * Cette méthode crée trois tables : Auteurs, Livre et Editeur.
     * Elle gère également les relations entre ces tables via des clés étrangères.
     */
    public static void createTables() {
        try (Connection connection = DatabaseConnection.getConnection();
             Statement statement = connection.createStatement()) {

            // Création de la table Auteurs
            String createAuteursTable = "CREATE TABLE Auteurs (" +
                    "Code VARCHAR(255) PRIMARY KEY, " +
                    "Nom VARCHAR(255), " +
                    "Prenom VARCHAR(255));";
            statement.executeUpdate(createAuteursTable);

            // Création de la table Editeur
            String createEditeurTable = "CREATE TABLE Editeur (" +
                    "Code VARCHAR(255) PRIMARY KEY, " +
                    "Nom VARCHAR(255), " +
                    "Adresse VARCHAR(255), " +
                    "Ville VARCHAR(255));";
            statement.executeUpdate(createEditeurTable);

            // Création de la table Livre avec des clés étrangères référençant les tables Auteurs et Editeur
            String createLivreTable = "CREATE TABLE Livre (" +
                    "ISBN VARCHAR(255) PRIMARY KEY, " +
                    "Titre VARCHAR(255), " +
                    "URLImage VARCHAR(255), " +
                    "Auteur VARCHAR(255), " +
                    "Editeur VARCHAR(255), " +
                    "FOREIGN KEY (Auteur) REFERENCES Auteurs(Code), " +
                    "FOREIGN KEY (Editeur) REFERENCES Editeur(Code));";
            statement.executeUpdate(createLivreTable);

            System.out.println("Tables créées avec succès.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

