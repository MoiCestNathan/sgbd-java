package main.tp;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MenuController {

    @FXML
    private void openListeLivres() {
        openWindow("LivresListe.fxml", "Liste des Livres");
    }

    @FXML
    private void openFonction1() {
        openWindow("RechercheLivre.fxml", "Recherche de Livre par ISBN");    }

    @FXML
    private void openFonction2() {
        openWindow("AjoutLivre.fxml", "Ajout de livre");
    }

    private void openWindow(String fxmlFile, String title) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource(fxmlFile));
            Stage stage = new Stage();
            stage.setTitle(title);
            stage.setScene(new Scene(root));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
