package main.tp;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class LivresListeController {

    @FXML
    private TableView<LivreInfo> tableView;

    @FXML
    private TableColumn<LivreInfo, String> isbnColumn;
    @FXML
    private TableColumn<LivreInfo, String> titreColumn;
    @FXML
    private TableColumn<LivreInfo, String> auteurNomColumn;
    @FXML
    private TableColumn<LivreInfo, String> auteurPrenomColumn;
    @FXML
    private TableColumn<LivreInfo, String> editeurNomColumn;
    @FXML
    private TableColumn<LivreInfo, String> editeurVilleColumn;

    @FXML
    private void initialize() {
        isbnColumn.setCellValueFactory(new PropertyValueFactory<>("isbn"));
        titreColumn.setCellValueFactory(new PropertyValueFactory<>("titre"));
        auteurNomColumn.setCellValueFactory(new PropertyValueFactory<>("nomAuteur"));
        auteurPrenomColumn.setCellValueFactory(new PropertyValueFactory<>("prenomAuteur"));
        editeurNomColumn.setCellValueFactory(new PropertyValueFactory<>("nomEditeur"));
        editeurVilleColumn.setCellValueFactory(new PropertyValueFactory<>("villeEditeur"));

        // Chargez les données et ajoutez-les à la TableView
        tableView.setItems(FXCollections.observableArrayList(new DatabaseConnection().getLivresInfos()));
    }
}
