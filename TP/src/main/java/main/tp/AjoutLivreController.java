package main.tp;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class AjoutLivreController {

    @FXML
    private TextField isbnTextField, titreTextField, nomAuteurTextField, prenomAuteurTextField, nomEditeurTextField, villeEditeurTextField;

    @FXML
    private Label resultatLabel;

    private DatabaseConnection databaseConnection = new DatabaseConnection();

    @FXML
    private void handleAjouterLivreClick() {
        String isbn = isbnTextField.getText();
        String titre = titreTextField.getText();
        String nomAuteur = nomAuteurTextField.getText();
        String prenomAuteur = prenomAuteurTextField.getText();
        String nomEditeur = nomEditeurTextField.getText();
        String villeEditeur = villeEditeurTextField.getText();

        if (databaseConnection.isAuteurExists(nomAuteur, prenomAuteur) && databaseConnection.isEditeurExists(nomEditeur, villeEditeur)) {
            databaseConnection.insererLivre(new LivreInfo(isbn, titre, nomAuteur, prenomAuteur, nomEditeur, villeEditeur));
            resultatLabel.setText("Livre ajouté avec succès.");
        } else {
            resultatLabel.setText("Erreur : Auteur ou Éditeur non trouvé.");
        }
    }
}
