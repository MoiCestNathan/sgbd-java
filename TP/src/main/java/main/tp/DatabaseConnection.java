package main.tp;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DatabaseConnection {

    /**
     * Établit et retourne une connexion à la base de données.
     *
     * Les informations de connexion sont récupérées à partir de la classe databaseConfig.
     *
     * @return Connection - un objet Connection à la base de données.
     * @throws SQLException - en cas d'échec de la connexion.
     */
    public static Connection getConnection() throws SQLException {
        // Utilise DriverManager pour obtenir une connexion à la base de données
        // Les paramètres URL, USER et PASSWORD sont définis dans la classe databaseConfig
        return DriverManager.getConnection(DatabaseConfig.URL, DatabaseConfig.USER, DatabaseConfig.PASSWORD);
    }

    public List<LivreInfo> getLivresInfos() {
        List<LivreInfo> livres = new ArrayList<>();

        String query = "SELECT Livre.ISBN, Livre.Titre, Auteurs.Nom AS AuteurNom, Auteurs.Prenom AS AuteurPrenom, Editeur.Nom AS EditeurNom, Editeur.Ville AS EditeurVille " +
                "FROM Livre " +
                "JOIN Auteurs ON Livre.Auteur = Auteurs.Code " +
                "JOIN Editeur ON Livre.Editeur = Editeur.Code";

        try (Connection connection = DatabaseConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                String isbn = resultSet.getString("ISBN");
                String titre = resultSet.getString("Titre");
                String auteurNom = resultSet.getString("AuteurNom");
                String auteurPrenom = resultSet.getString("AuteurPrenom");
                String editeurNom = resultSet.getString("EditeurNom");
                String editeurVille = resultSet.getString("EditeurVille");

                LivreInfo livreInfo = new LivreInfo(isbn, titre, auteurNom, auteurPrenom, editeurNom, editeurVille);
                livres.add(livreInfo);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return livres;
    }

    public LivreInfo rechercherLivreParISBN(String isbn) {
        LivreInfo livre = null;

        String query = "SELECT Livre.ISBN, Livre.Titre, Auteurs.Nom AS AuteurNom, Auteurs.Prenom AS AuteurPrenom, Editeur.Nom AS EditeurNom, Editeur.Ville AS EditeurVille " +
                "FROM Livre " +
                "JOIN Auteurs ON Livre.Auteur = Auteurs.Code " +
                "JOIN Editeur ON Livre.Editeur = Editeur.Code " +
                "WHERE Livre.ISBN = ?";

        try (Connection connection = DatabaseConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            preparedStatement.setString(1, isbn);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                String titre = resultSet.getString("Titre");
                String auteurNom = resultSet.getString("AuteurNom");
                String auteurPrenom = resultSet.getString("AuteurPrenom");
                String editeurNom = resultSet.getString("EditeurNom");
                String editeurVille = resultSet.getString("EditeurVille");

                livre = new LivreInfo(isbn, titre, auteurNom, auteurPrenom, editeurNom, editeurVille);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return livre;
    }

    public boolean isAuteurExists(String nom, String prenom) {
        String query = "SELECT COUNT(*) FROM Auteur WHERE Nom = ? AND Prenom = ?";
        try (Connection connection = DatabaseConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            preparedStatement.setString(1, nom);
            preparedStatement.setString(2, prenom);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    int count = resultSet.getInt(1);
                    return count > 0;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isEditeurExists(String nom, String ville) {
        String query = "SELECT COUNT(*) FROM Editeur WHERE Nom = ? AND Ville = ?";
        try (Connection connection = DatabaseConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            preparedStatement.setString(1, nom);
            preparedStatement.setString(2, ville);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    int count = resultSet.getInt(1);
                    return count > 0;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void insererLivre(LivreInfo livre) {
        String query = "INSERT INTO Livre (ISBN, Titre, CodeAuteur, CodeEditeur) VALUES (?, ?, ?, ?)";

        // Récupérer les codes d'auteur et d'éditeur à partir de leurs noms et prénoms/nom de ville
        int codeAuteur = getCodeAuteur(livre.getNomAuteur(), livre.getPrenomAuteur());
        int codeEditeur = getCodeEditeur(livre.getNomEditeur(), livre.getVilleEditeur());

        try (Connection connection = DatabaseConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            preparedStatement.setString(1, livre.getIsbn());
            preparedStatement.setString(2, livre.getTitre());
            preparedStatement.setInt(3, codeAuteur);
            preparedStatement.setInt(4, codeEditeur);

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("La création du livre a échoué, aucune ligne ajoutée.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private int getCodeAuteur(String nom, String prenom) {
        String query = "SELECT Code FROM Auteur WHERE Nom = ? AND Prenom = ?";
        try (Connection connection = DatabaseConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            preparedStatement.setString(1, nom);
            preparedStatement.setString(2, prenom);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt("Code");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1; // -1 indique qu'aucun code n'a été trouvé
    }

    private int getCodeEditeur(String nom, String ville) {
        String query = "SELECT Code FROM Editeur WHERE Nom = ? AND Ville = ?";
        try (Connection connection = DatabaseConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            preparedStatement.setString(1, nom);
            preparedStatement.setString(2, ville);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt("Code");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1; // -1 indique qu'aucun code n'a été trouvé
    }





}
