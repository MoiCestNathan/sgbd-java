package main.tp;

public class DatabaseConfig {

    /**
     * URL de la base de données.
     *
     * Cette URL indique l'adresse de la base de données, le type de base de données (MySQL),
     * le serveur hôte (localhost), le port (3306 par défaut pour MySQL) et le nom de la base de données (tp2database).
     */
    public static final String URL = "jdbc:mysql://localhost:3306/tp2database";

    /**
     * Nom d'utilisateur pour se connecter à la base de données.
     *
     */
    public static final String USER = "root";

    /**
     * Mot de passe pour se connecter à la base de données.
     *
     */
    public static final String PASSWORD = "password";
}


