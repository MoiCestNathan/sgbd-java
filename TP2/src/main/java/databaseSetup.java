import java.sql.Connection;
import java.sql.Statement;

public class databaseSetup {

    /**
     * Crée les tabdles nécessaires dans la base de onnées.
     *
     * Cette méthode crée deux tables : Citoyen et Foyer.
     * Elle gère également les relations entre ces tables via des clés étrangères.
     */
    public static void createTables() {
        try (Connection connection = databaseConnection.getConnection();
             Statement statement = connection.createStatement()) {

            // Création de la table Citoyen sans clé étrangère pour l'instant
            String createCitoyenTable = "CREATE TABLE Citoyen (" +
                    "NSS VARCHAR(255) PRIMARY KEY, " +
                    "Nom VARCHAR(255), " +
                    "Prenom VARCHAR(255), " +
                    "Sex CHAR(1), " +
                    "Conjoint VARCHAR(255), " +
                    "Foyer VARCHAR(255));";
            statement.executeUpdate(createCitoyenTable);

            // Création de la table Foyer avec des clés étrangères référençant la table Citoyen
            String createFoyerTable = "CREATE TABLE Foyer (" +
                    "Code VARCHAR(255) PRIMARY KEY, " +
                    "Pere VARCHAR(255), " +
                    "Mere VARCHAR(255), " +
                    "Adresse VARCHAR(255), " +
                    "FOREIGN KEY (Pere) REFERENCES Citoyen(NSS), " +
                    "FOREIGN KEY (Mere) REFERENCES Citoyen(NSS));";
            statement.executeUpdate(createFoyerTable);

            // Ajout de la clé étrangère dans la table Citoyen pour référencer la table Foyer
            String addForeignKeyToCitoyen = "ALTER TABLE Citoyen " +
                    "ADD FOREIGN KEY (Foyer) REFERENCES Foyer(Code);";
            statement.executeUpdate(addForeignKeyToCitoyen);

            System.out.println("Tables créées avec succès.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
