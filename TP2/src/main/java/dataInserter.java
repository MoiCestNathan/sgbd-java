import java.sql.Connection;
import java.sql.PreparedStatement;

public class dataInserter {

    /**
     * Insère les données initiales dans les tables Citoyen et Foyer.
     */
    public static void insertData() {
        try (Connection connection = databaseConnection.getConnection()) {

            // Préparation de la requête pour insérer des citoyens
            String insertCitoyenQuery = "INSERT INTO Citoyen (NSS, Nom, Prenom, Sex, Conjoint, Foyer) VALUES (?, ?, ?, ?, ?, ?);";
            try (PreparedStatement insertCitoyenStmt = connection.prepareStatement(insertCitoyenQuery)) {
                // Insérer le premier citoyen sans foyer
                insertCitoyenStmt.setString(1, "123456789");
                insertCitoyenStmt.setString(2, "Fontaine");
                insertCitoyenStmt.setString(3, "Nathan");
                insertCitoyenStmt.setString(4, "M");
                insertCitoyenStmt.setString(5, null); // Pas de conjoint
                insertCitoyenStmt.setString(6, null); // Pas de foyer
                insertCitoyenStmt.executeUpdate();

                // Insérer le deuxième citoyen sans foyer
                insertCitoyenStmt.setString(1, "987654321");
                insertCitoyenStmt.setString(2, "Petit");
                insertCitoyenStmt.setString(3, "Caroline");
                insertCitoyenStmt.setString(4, "F");
                insertCitoyenStmt.setString(5, null); // Pas de conjoint
                insertCitoyenStmt.setString(6, null); // Pas de foyer
                insertCitoyenStmt.executeUpdate();
            }

            // Préparation de la requête pour insérer un foyer
            String insertFoyerQuery = "INSERT INTO Foyer (Code, Pere, Mere, Adresse) VALUES (?, ?, ?, ?);";
            try (PreparedStatement insertFoyerStmt = connection.prepareStatement(insertFoyerQuery)) {
                // Insérer un foyer
                insertFoyerStmt.setString(1, "F1");
                insertFoyerStmt.setString(2, "123456789"); // NSS du père
                insertFoyerStmt.setString(3, "987654321"); // NSS de la mère
                insertFoyerStmt.setString(4, "229 Avenue de Grammont");
                insertFoyerStmt.executeUpdate();
            }

            // Préparation de la requête pour mettre à jour le foyer des citoyens
            String updateCitoyenFoyerQuery = "UPDATE Citoyen SET Foyer = ? WHERE NSS = ?;";
            try (PreparedStatement updateCitoyenFoyerStmt = connection.prepareStatement(updateCitoyenFoyerQuery)) {
                // Mettre à jour le foyer du premier citoyen
                updateCitoyenFoyerStmt.setString(1, "F1");
                updateCitoyenFoyerStmt.setString(2, "123456789");
                updateCitoyenFoyerStmt.executeUpdate();

                // Mettre à jour le foyer du deuxième citoyen
                updateCitoyenFoyerStmt.setString(1, "F1");
                updateCitoyenFoyerStmt.setString(2, "987654321");
                updateCitoyenFoyerStmt.executeUpdate();
            }

            System.out.println("Données insérées avec succès.");

        } catch (Exception e) {
            // Afficher l'exception en cas d'erreur
            e.printStackTrace();
        }
    }
}
